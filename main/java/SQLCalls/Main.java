package SQLCalls;

public class Main {

    public static void main(String[] args) {
        ConnectionToDB.mySQLClient();
        ConnectionToDB.amountOfPeople();
        ConnectionToDB.averageAgeOfPeople();
        ConnectionToDB.sortedListOfLastName();
        ConnectionToDB.sortedListOfLastNameAndAmountOfPeopleWithSameLastName();
        ConnectionToDB.listOfLastNamesWithLetterB();
        ConnectionToDB.listOfPeopleWithoutAdress();
        ConnectionToDB.listOfPeopleWhoLiveOnProspectPravdyUnderEighteen();
        ConnectionToDB.listOfStreetsWithAmountOfPeople();
        ConnectionToDB.listOfStreetsWithSixLetters();
        ConnectionToDB.listOfStreetsWithLessThanThreeResidents();
    }
}
