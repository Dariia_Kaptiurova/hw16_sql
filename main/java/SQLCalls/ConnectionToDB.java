package SQLCalls;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConnectionToDB {

    private static final String URL = "jdbc:ucanaccess://C://Users//Admin//Documents//MySQL.accdb";

    private static Connection connection;
    private static Statement statement;
    private static ResultSet resultSet;

    public static void mySQLClient() {
        try {
            connection = DriverManager.getConnection(URL);
            statement = connection.createStatement();

        } catch (SQLException sqlEx) {
            System.out.println("Connection Failed! Check output console");
            sqlEx.printStackTrace();
        }

        if (connection != null) {
            System.out.println("You successfully connect to data base!");
        } else {
            System.out.println("Failed to make connection!");
        }
    }


    public static int amountOfPeople() {
        String query = "SELECT COUNT(*) FROM Person";
        int count = 0;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                count = resultSet.getInt(1);
            }
            System.out.println("Amount of people: " + count);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }


    public static int averageAgeOfPeople() {
        String query = "SELECT avg(age) FROM person";
        int averageAge = 0;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                averageAge = resultSet.getInt(1);
            }
            System.out.println("Average age of people: " + averageAge);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return averageAge;
    }


    public static List<String> sortedListOfLastName() {
        String query = "SELECT DISTINCT lastName FROM Person ORDER BY lastName";
        List<String> people = new ArrayList<>();
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                people.add(resultSet.getString(1));
            }
            System.out.println("List of people sorted by last names: " + people);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return people;
    }


    public static Map<String, String> sortedListOfLastNameAndAmountOfPeopleWithSameLastName() {
        String query = "SELECT lastName, COUNT(*) FROM person GROUP BY lastName";
        Map<String, String> map = new HashMap<>();
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                map.put(resultSet.getString(1), resultSet.getString(2));
            }
            System.out.println("List of last names and amount of people with the same last name: " + map);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return map;
    }


    public static List<String> listOfLastNamesWithLetterB() {
        String query = "SELECT lastName FROM person WHERE lastName LIKE '_%б%_'";
        List<String> people = new ArrayList<>();
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                people.add(resultSet.getString(1));
            }
            System.out.println("List of last names with letter b in the middle: " + people);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return people;
    }


    public static List<String> listOfPeopleWithoutAdress() {
        String query = "SELECT * FROM person WHERE idStreet IS NULL";
        ArrayList<String> arrayList = new ArrayList<>();
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {

                String persons = "\n id: " + resultSet.getString(1) + ", " + resultSet.getString(2) + " " +
                        resultSet.getString(3) + ", age " + resultSet.getInt("age");
                arrayList.add("List of people without adress: " + persons);
            }
            System.out.println(arrayList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return arrayList;
    }


    public static List<String> listOfPeopleWhoLiveOnProspectPravdyUnderEighteen() {
        String query = "SELECT * FROM person JOIN street ON person.idStreet = street.id\n" +
                "WHERE UPPER(NameOfStreet) LIKE '%ПРОСПЕКТ ПРАВДЫ%' AND person.age < 18";
        List<String> arrayList = new ArrayList<>();
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {

                String persons = "\n id: " + resultSet.getString(1) + ", " + resultSet.getString(2) + " " +
                        resultSet.getString(3) + ", age " + resultSet.getInt("age");
                arrayList.add("List of people, who live on Prospect Pravdy under eighteen years old: " + persons);
            }
            System.out.println(arrayList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return arrayList;
    }


    public static Map<String, String> listOfStreetsWithAmountOfPeople() {
        String query = "SELECT nameOfStreet, COUNT(idPerson) FROM street JOIN person ON street.id = person.idStreet GROUP BY nameOfStreet";
        Map<String, String> map = new HashMap<>();
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                map.put(resultSet.getString(1), resultSet.getString(2));
            }
            System.out.println("List of streets with amount of people " + map);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return map;
    }

    public static List<String> listOfStreetsWithSixLetters() {
        String query = "SELECT * FROM street WHERE LENGTH(nameOfStreet) =6";
        List<String> arrayList = new ArrayList<>();
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                arrayList.add(resultSet.getString(1));
            }
            System.out.println("List of streets with six letters: " + arrayList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return arrayList;
    }


    public static List<String> listOfStreetsWithLessThanThreeResidents() {
        String query = "SELECT nameOfStreet FROM street JOIN person ON street.id = person.idStreet" +
                " GROUP BY nameOfStreet" +
                " HAVING COUNT(person.idStreet) < 3";
        List<String> arrayList = new ArrayList<>();
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                arrayList.add(resultSet.getString(1));
            }
            System.out.println("List of streets with less than three residents: " + arrayList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return arrayList;
    }
}
