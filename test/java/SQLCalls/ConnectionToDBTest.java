package SQLCalls;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class ConnectionToDBTest {

    @BeforeEach
public void connectToDB() {
        ConnectionToDB.mySQLClient();
    }

    @Test
    void amountOfPeople_ShouldCountAmountOfAllPeopleInDatabase() {

        //given
        int expected = 12;
        int expected1 = 0;

        //when
        int actual = ConnectionToDB.amountOfPeople();

        //then
        assertEquals(expected, actual);
        assertNotEquals(expected1, actual);
    }

    @Test
    void averageAgeOfPeople_ShouldCountAnAverageAgeOfPeopleInDatabase() {

        //given
        int expected = 22;
        int expected1 = 0;

        //when
        int actual = ConnectionToDB.averageAgeOfPeople();

        //then
        assertEquals(expected, actual);
        assertNotEquals(expected1, actual);
    }

    @Test
    void sortedListOfLastName_ShouldShowSortedListOfLastNames() {

        //given
        String expected = "[Баранец, Белобородько, Бульба, Горишний, Зозуля, Каптюрова, Луценко, Мацюк, Смирнов, Таптунов, Топчу]";
        List<String> expected1 = null;

        //when
        List<String> actual = ConnectionToDB.sortedListOfLastName();

        //then
        assertEquals(expected, actual.toString());
        assertNotEquals(expected1, actual.toString());
    }

    @Test
    void sortedListOfLastNameAndAmountOfPeopleWithSameLastName_ShouldShowSortedListOfLastNamesAndAmountOfPeopleWithSameLastName() {

        //given
        String expected = "{Баранец=1, Белобородько=1, Зозуля=1, Топчу=1, Луценко=1, Таптунов=1, Каптюрова=1, Смирнов=1, Бульба=2, Горишний=1, Мацюк=1}";
        List<String> expected1 = null;

        //when
        Map<String, String> actual = ConnectionToDB.sortedListOfLastNameAndAmountOfPeopleWithSameLastName();

        //then
        assertEquals(expected, actual.toString());
        assertNotEquals(expected1, actual.toString());
    }

    @Test
    void listOfLastNamesWithLetterB_ShouldShowListOfLastNamesWithLetterB() {

        //given
        String expected = "[Белобородько, Бульба, Бульба]";
        List<String> expected1 = null;

        //when
        List<String> actual = ConnectionToDB.listOfLastNamesWithLetterB();

        //then
        assertEquals(expected, actual.toString());
        assertNotEquals(expected1, actual.toString());
    }

    @Test
    void listOfPeopleWithoutAdress_ShouldShowListOfPeopleWithoutAdress() {

        //given
        String expected = "[List of people without adress: \n" +
                " id: 7, Валерия Зозуля, age 23, List of people without adress: \n" +
                " id: 9, Игорь Мацюк, age 15, List of people without adress: \n" +
                " id: 11, Тарас Бульба, age 12]";
        List<String> expected1 = null;

        //when
        List<String> actual = ConnectionToDB.listOfPeopleWithoutAdress();

        //then
        assertEquals(expected, actual.toString());
        assertNotEquals(expected1, actual.toString());
    }

    @Test
    void listOfPeopleWhoLiveOnProspectPravdyUnderEighteen_ShouldShowPeopleWhoLiveOnProspectPravdyAndHaveUnderEighteenYearsOld() {

        //given
        String expected = "[List of people, who live on Prospect Pravdy under eighteen years old: \n" +
                " id: 10, Евгений Белобородько, age 9]";
        List<String> expected1 = null;

        //when
        List<String> actual = ConnectionToDB.listOfPeopleWhoLiveOnProspectPravdyUnderEighteen();

        //then
        assertEquals(expected, actual.toString());
        assertNotEquals(expected1, actual.toString());
    }

    @Test
    void listOfStreetsWithAmountOfPeople_ShouldShowListOfStreetsWithAmountOfPeople() {

        //given
        String expected = "{Сковороды=2, Проспект Правды=3, Бажана=1, Стуса=2, Шевченка=1}";
        List<String> expected1 = null;

        //when
        Map<String, String> actual = ConnectionToDB.listOfStreetsWithAmountOfPeople();

        //then
        assertEquals(expected, actual.toString());
        assertNotEquals(expected1, actual.toString());
    }

    @Test
    void listOfStreetsWithSixLetters_ShouldShowListOfStreetsWithSixLetters() {

        //given
        String expected = "[Бажана]";
        List<String> expected1 = null;

        //when
        List<String> actual = ConnectionToDB.listOfStreetsWithSixLetters();

        //then
        assertEquals(expected, actual.toString());
        assertNotEquals(expected1, actual.toString());
    }

    @Test
    void listOfStreetsWithLessThanThreeResidents_ShouldShowListOfStreetsWithLessThanThreeResidents() {

        //given
        String expected = "[Сковороды, Шевченка, Стуса, Бажана]";
        List<String> expected1 = null;

        //when
        List<String> actual = ConnectionToDB.listOfStreetsWithLessThanThreeResidents();

        //then
        assertEquals(expected, actual.toString());
        assertNotEquals(expected1, actual.toString());
    }
}